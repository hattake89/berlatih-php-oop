<?php
class Ape extends Animal 
{
  public $legs;
  public $cold_blooded;
  public $name ;
  
  public function __construct($name) 
  {
    $this->name= $name;
    $this->legs = 2;
    $this->cold_blooded = "false";
  }

  public function yell(){
    return "Auoooo";
  }
  
  
}
?>