<?php
require 'Animal.php';
require 'Frog.php';
require 'Ape.php';

$sheep = new Animal('shaun');
echo $sheep->get_name()."<br>"; // "shaun"
echo $sheep->get_legs()."<br>"; // 2
echo $sheep->get_cold_bloded()."<br><br>"; // false

$sungokong  = new Ape('kera sakti');
echo $sungokong->get_name()."<br>"; // "shaun"
echo $sungokong->get_legs()."<br>"; // 2
echo $sungokong->get_cold_bloded()."<br>"; // false
echo $sungokong->yell()."<br><br>"; // false

$kodok = new Frog('buduk');
echo $kodok->get_name()."<br>"; // "shaun"
echo $kodok->get_legs()."<br>"; // 2
echo $kodok->get_cold_bloded()."<br>"; // false
echo $kodok->jump()."<br><br>"; // false
?>